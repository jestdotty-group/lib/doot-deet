const assert = require('assert')
const Tone = require('../src/tone')
const Pitch = require('../src/pitch')
const Modulator = require('../src/modulator')
const effects = require('../src/effects')

const assertData = (datum, instrument, volume, hz, left, right, effects, start, end)=>{
	assert.equal(datum.instrument, instrument)
	assert.equal(datum.volume.length, volume.length)
	for(let i=0; i<volume.length; i++){
		assert.equal(datum.volume[i].v, volume[i].v)
		assert.equal(datum.volume[i].d, volume[i].d)
	}
	if(typeof hz === 'number') assert.equal((datum.pitch.hz*100 | 0)/100, hz)
	assert.equal(datum.left, left)
	assert.equal(datum.right, right)
	assert.deepEqual(datum.effects, effects)
	assert.equal(datum.start, start)
	assert.equal(datum.end, end)
}

describe('tone', ()=>{
	describe('constructor', ()=>{
		it('duplicate', ()=>{
			const tone = new Tone()
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 16.35, undefined, undefined, undefined, 0, 0)

			const duplicate = new Tone(tone)
			assert.notEqual(tone, duplicate)
			assertData(duplicate, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 16.35, undefined, undefined, undefined, 0, 0)
		})
		it('changed', ()=>{
			const tone = new Tone()
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 16.35, undefined, undefined, undefined, 0, 0)

			const duplicate = new Tone({
				...tone,
				instrument: 'sine',
				left: true,
				right: true,
				effects: [],
				start: 0.05,
				end: -0.05
			})
			assertData(duplicate, 'sine', [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 16.35, true, true, [], 0.05, -0.05)
		})
	})
	describe('from object', ()=>{
		it('pitch number', ()=>{
			const tone = Tone.from({pitch: 64})
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 659.25, undefined, undefined, undefined, 0, 0)
		})
		it('pitch object', ()=>{
			const tone = Tone.from({pitch: {hz: 125}})
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 125, undefined, undefined, undefined, 0, 0)
		})
		it('pitch array', ()=>{
			const tone = Tone.from({pitch: [
				[0.1, 30],
				[0.5, new Pitch({note: 40})]
			]})
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, undefined, undefined, 0, 0)
			assert.equal(tone.pitch.length, 2)
			assert.equal(tone.pitch[0].d, 0.1)
			assert.equal((tone.pitch[0].v*100 |0)/100, 92.49)
			assert.equal(tone.pitch[1].d, 0.5)
			assert.equal((tone.pitch[1].v*100 |0)/100, 164.81)
		})
		it('pitch modulator', ()=>{
			const tone = Tone.from({pitch: new Modulator(
				[0.1, 30],
				{d: 0.5, v: new Pitch({note: 40})}
			)})
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, undefined, undefined, 0, 0)
			assert.equal(tone.pitch.length, 2)
			assert.equal(tone.pitch[0].d, 0.1)
			assert.equal((tone.pitch[0].v*100 |0)/100, 92.49)
			assert.equal(tone.pitch[1].d, 0.5)
			assert.equal((tone.pitch[1].v*100 |0)/100, 164.81)
		})
		it('volume object', ()=>{
			const tone = Tone.from({volume: [
				{v:1, d:2},
				{v:0, d:1},
			]})
			assertData(tone, undefined, [
				{v: 1, d: 2},
				{v: 0, d: 1},
			], 16.35, undefined, undefined, undefined, 0, 0)
		})
		describe('relativity', ()=>{
			const pitch = [[1, 440]]
			const volume = [[0.5, 1]]
			it('raw', ()=>{
				const tone = Tone.from({pitch, volume})
				assert.equal(tone.duration, 1)
				assert.equal(tone.pitch.duration, 1)
				assert.equal(tone.volume.duration, 0.5)
				assert.equal(tone.start, 0)
				assert.equal(tone.end, 0)
			})
			it('pitch', ()=>{
				const tone = Tone.from({pitch, volume, rPitch: true})
				assert.equal(tone.duration, 0.5)
				assert.equal(tone.pitch.duration, 0.5)
				assert.equal(tone.volume.duration, 0.5)
				assert.equal(tone.start, 0)
				assert.equal(tone.end, 0)
			})
			it('volume', ()=>{
				const tone4 = Tone.from({pitch, volume, rVolume: true})
				assert.equal(tone4.duration, 1)
				assert.equal(tone4.pitch.duration, 1)
				assert.equal(tone4.volume.duration, 1)
				assert.equal(tone4.start, 0)
				assert.equal(tone4.end, 0)
			})
			it('rPitch', ()=>{
				const tone = Tone.from({rPitch: new Pitch({note: 5})})
				assert.equal(tone.duration, 0.1)
				assert.equal(tone.pitch.number, 5)
				assert.equal(tone.pitch.duration, undefined)
				assert.equal(tone.volume.duration, 0.1)
				assert.equal(tone.start, 0)
				assert.equal(tone.end, 0)
			})
			it('rVolume', ()=>{
				const tone = Tone.from({rVolume: new Modulator({d:0.5, v: 1})})
				assert.equal(tone.duration, 0.5)
				assert.equal(tone.pitch.number, 0)
				assert.equal(tone.pitch.duration, undefined)
				assert.equal(tone.volume.duration, 0.5)
				assert.equal(tone.start, 0)
				assert.equal(tone.end, 0)
			})
			it('start', ()=>{
				const tone = Tone.from({pitch, volume, rStart: 0.5})
				assert.equal(tone.duration, 1.5)
				assert.equal(tone.pitch.duration, 1)
				assert.equal(tone.volume.duration, 0.5)
				assert.equal(tone.start, 0.5)
				assert.equal(tone.end, 0)
			})
			it('end', ()=>{
				const tone = Tone.from({pitch, volume, rEnd: -0.5})
				assert.equal(tone.duration, 0.5)
				assert.equal(tone.pitch.duration, 1)
				assert.equal(tone.volume.duration, 0.5)
				assert.equal(tone.start, 0)
				assert.equal(tone.end, -0.5)
			})
			it('all', ()=>{
				const tone = Tone.from({rPitch: pitch, rVolume: volume, rStart: 0.25, rEnd: -0.5})
				assert.equal(tone.duration, 0.375)
				assert.equal(tone.pitch.duration, 0.5)
				assert.equal(tone.volume.duration, 0.5)
				assert.equal(tone.start, 0.125)
				assert.equal(tone.end, -0.25)
			})
		})
	})
	it('mapPitch', ()=>{
		const increaseByOne = p=> new Pitch({note: p.number+1})

		const pitch = new Pitch({note: 13})
		const mappedPitch = Tone.mapPitch(pitch, increaseByOne)
		assert.equal(pitch.number, 13)
		assert.equal(mappedPitch.number, 14)

		const modulator = new Modulator([0.1, pitch], [0.2, Pitch.from(20)])
		const mappedModulator = Tone.mapPitch(modulator, increaseByOne)
		assert.equal(modulator.length, 2)
		assert.equal(modulator[0].v.number, 13)
		assert.equal(modulator[1].v.number, 20)
		assert.equal(mappedModulator.length, 2)
		assert.equal(mappedModulator[0].v.number, 14)
		assert.equal(mappedModulator[1].v.number, 21)
	})
	describe('duration', ()=>{
		it('volume', ()=>{
			const tone = Tone.from({volume: [{v:0, d:1}]})
			assert.equal(tone.duration, 1)
		})
		it('end', ()=>{
			const tone = Tone.from({volume: [{v:0, d:1}], end: -0.5})
			assert.equal(tone.duration, 0.5)
		})
		it('start', ()=>{
			const tone = Tone.from({volume: [{v:0, d:1}], start: 0.5})
			assert.equal(tone.duration, 1.5)
		})
	})
	describe('extraDuration', ()=>{
		it('none', ()=>{
			const tone = new Tone()
			assert.equal(tone.extraDuration, 0)
		})
		it('empty effects', ()=>{
			const tone = new Tone({effects: []})
			assert.equal(tone.extraDuration, 0)
		})
		it('no duration effect', ()=>{
			const tone = new Tone({effects: [new effects.Distortion(5)]})
			assert.equal(tone.extraDuration, 0)
		})
		it('duration effect', ()=>{
			const tone = new Tone({effects: [new effects.Reverb(2)]})
			assert.equal(tone.extraDuration, 2)
		})
		it('duration and no duration effects', ()=>{
			const tone = new Tone({effects: [new effects.Reverb(2), new effects.Distortion(5)]})
			assert.equal(tone.extraDuration, 2)
		})
	})
	it('split', ()=>{
		const tone = new Tone()
		assertData(tone, undefined, [
			{v: 1, d: 0.04},
			{v: 1, d: 0.04},
			{v: 0, d: 0.02},
		], 16.35, undefined, undefined, undefined, 0, 0)

		const split = tone.split({}, {pitch: 5, rStart: 0.5, rEnd: -0.5})
		assertData(split[0], undefined, [
			{v: 1, d: 0.04},
			{v: 1, d: 0.04},
			{v: 0, d: 0.02},
		], 16.35, undefined, undefined, undefined, 0, 0)
		assertData(split[1], undefined, [
			{v: 1, d: 0.04},
			{v: 1, d: 0.04},
			{v: 0, d: 0.02},
		], 21.82, undefined, undefined, undefined, 0.05, -0.05)
	})
	describe('chord', ()=>{
		it('pitch', ()=>{
			const tone = new Tone({start: 0.1, end: 0.2})
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 16.35, undefined, undefined, undefined, 0.1, 0.2)

			const chords = tone.chord(1, 2, 3)
			assert.equal(chords.length, 3)
			assertData(chords[0], undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 17.32, undefined, undefined, undefined, 0.1, 0.2)
			assertData(chords[1], undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 18.35, undefined, undefined, undefined, 0.1, 0.2)
			assertData(chords[2], undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 19.44, undefined, undefined, undefined, 0.1, 0.2)
		})
		it('modulator', ()=>{
			const tone = Tone.from({start: 0.1, end: 0.2, pitch: [[0.1, 30], [0.2, 40], [0.3, 50]]})
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, undefined, undefined, 0.1, 0.2)
			assert.equal(tone.pitch.length, 3)
			assert.equal(tone.pitch[0].d, 0.1)
			assert.equal(tone.pitch[0].v.number, 30)
			assert.equal(tone.pitch[1].d, 0.2)
			assert.equal(tone.pitch[1].v.number, 40)
			assert.equal(tone.pitch[2].d, 0.3)
			assert.equal(tone.pitch[2].v.number, 50)

			const chords = tone.chord(
				(pitch, i)=> pitch.number + (i%2===0? -i: i),
				0,
				(pitch, i)=> pitch.number + (i%2===0? i: -i)
			)
			assert.equal(chords.length, 3)
			assertData(chords[0], undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, undefined, undefined, 0.1, 0.2)
			assert.equal(chords[0].pitch.length, 3)
			assert.equal(chords[0].pitch[0].v.number, 30)
			assert.equal(chords[0].pitch[1].v.number, 41)
			assert.equal(chords[0].pitch[2].v.number, 48)
			assertData(chords[1], undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, undefined, undefined, 0.1, 0.2)
			assert.equal(chords[1].pitch.length, 3)
			assert.equal(chords[1].pitch[0].v.number, 30)
			assert.equal(chords[1].pitch[1].v.number, 40)
			assert.equal(chords[1].pitch[2].v.number, 50)
			assertData(chords[2], undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, undefined, undefined, 0.1, 0.2)
			assert.equal(chords[2].pitch.length, 3)
			assert.equal(chords[2].pitch[0].v.number, 30)
			assert.equal(chords[2].pitch[1].v.number, 39)
			assert.equal(chords[2].pitch[2].v.number, 52)
		})
	})
	describe('binaural', ()=>{
		it('number', ()=>{
			const tone = new Tone()
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 16.35, undefined, undefined, undefined, 0, 0)

			const [left, right] = tone.binaural(5, -5)
			assertData(left, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 21.35, true, undefined, undefined, 0, 0)
			assertData(right, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], 11.35, undefined, true, undefined, 0, 0)
		})
		it('modulator', ()=>{
			const tone = Tone.from({pitch: [[0.1, 30], [0.2, 40], [0.3, 50]]})
			assertData(tone, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, undefined, undefined, 0, 0)
			assert.equal(tone.pitch.length, 3)
			assert.equal(tone.pitch[0].d, 0.1)
			assert.equal((tone.pitch[0].v*100 |0)/100, 92.49)
			assert.equal(tone.pitch[1].d, 0.2)
			assert.equal((tone.pitch[1].v*100 |0)/100, 164.81)
			assert.equal(tone.pitch[2].d, 0.3)
			assert.equal((tone.pitch[2].v*100 |0)/100, 293.66)

			const [left, right] = tone.binaural(5, -5)
			assertData(left, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, true, undefined, undefined, 0, 0)
			assert.equal(left.pitch.length, 3)
			assert.equal(left.pitch[0].d, 0.1)
			assert.equal((left.pitch[0].v*100 |0)/100, 97.49)
			assert.equal(left.pitch[1].d, 0.2)
			assert.equal((left.pitch[1].v*100 |0)/100, 169.81)
			assert.equal(left.pitch[2].d, 0.3)
			assert.equal((left.pitch[2].v*100 |0)/100, 298.66)
			assertData(right, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, true, undefined, 0, 0)
			assert.equal(right.pitch.length, 3)
			assert.equal(right.pitch[0].d, 0.1)
			assert.equal((right.pitch[0].v*100 |0)/100, 87.49)
			assert.equal(right.pitch[1].d, 0.2)
			assert.equal((right.pitch[1].v*100 |0)/100, 159.81)
			assert.equal(right.pitch[2].d, 0.3)
			assert.equal((right.pitch[2].v*100 |0)/100, 288.66)

			const [left2, right2] = tone.binaural((pitch, i)=> pitch + (i%2===0? 1: -1), (pitch, i)=> pitch + (i%2===0? -2: 2))
			assertData(left2, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, true, undefined, undefined, 0, 0)
			assert.equal(left2.pitch.length, 3)
			assert.equal(left2.pitch[0].d, 0.1)
			assert.equal((left2.pitch[0].v*100 |0)/100, 93.49)
			assert.equal(left2.pitch[1].d, 0.2)
			assert.equal((left2.pitch[1].v*100 |0)/100, 163.81)
			assert.equal(left2.pitch[2].d, 0.3)
			assert.equal((left2.pitch[2].v*100 |0)/100, 294.66)
			assertData(right2, undefined, [
				{v: 1, d: 0.04},
				{v: 1, d: 0.04},
				{v: 0, d: 0.02},
			], false, undefined, true, undefined, 0, 0)
			assert.equal(right2.pitch.length, 3)
			assert.equal(right2.pitch[0].d, 0.1)
			assert.equal((right2.pitch[0].v*100 |0)/100, 90.49)
			assert.equal(right2.pitch[1].d, 0.2)
			assert.equal((right2.pitch[1].v*100 |0)/100, 166.81)
			assert.equal(right2.pitch[2].d, 0.3)
			assert.equal((right2.pitch[2].v*100 |0)/100, 291.66)
		})
	})
})
