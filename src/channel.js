const Tone = require('./tone')
const Modulator = require('./modulator')

class Channel extends Array{
	static looper(toneData, length){
		if(!length)
			length = Math.max(...Object.values(toneData).map(v=> v && !(v instanceof Modulator) && v instanceof Array? v.length: 1))
		return new Channel(...Array.from({length}, (_, i)=> Object.entries(toneData).reduce((obj, [key, value])=>{
			obj[key] = value && !(value instanceof Modulator) && value instanceof Array?  value[i%value.length]: value
			return obj
		}, {})))
	}
	static lcm(durations, precision=1){
		durations = durations.map(d=>({d, m: 1}))
		let totals = durations.map(({d, m})=> d*m)
		let min = Math.min(...totals)
		let max = Math.max(...totals)
		while(max - min > precision){
			durations.forEach(d=>{
				let diff = max - d.d*d.m
				while(diff > precision){
					d.m += 1 //XXX probably could mathematically figure this out but this is at least bugless
					diff = max - d.d*d.m
				}
			})
			totals = durations.map(({d, m})=> d*m)
			min = Math.min(...totals)
			max = Math.max(...totals)
		}
		return durations.map(({m})=> m)
	}
	static align(channels, precision){
		const multipliers = this.lcm(channels.map(c=> c.duration), precision)
		return multipliers.map((m, i)=> channels[i].repeat(m))
	}
	constructor(...data){
		super(...data.map(d=>
			d instanceof Channel || d instanceof Tone? d: //already expected data type
			d instanceof Array? new Channel(...d):
			Tone.from(d)
		))
	}
	get duration(){
		const unravel = (d, depth)=>{
			if(d.constructor !== Channel) return d.duration
			const durations = d.map(v=> unravel(v, depth+1))
			const parallel = depth % 2 === 0
			return parallel? Math.max(...durations): durations.reduce((r, v)=> r=r+v, 0)
		}
		return unravel(this, 1)
	}
	getPlayData(depth=0){
		return {
			parallel: depth % 2 === 0,
			data: this.map(d=> d instanceof Channel? d.getPlayData(depth+1): d)
		}
	}
	forEachTone(fn){
		const unravel = (d, i, a)=> d instanceof Channel? d.forEachTone(unravel): fn(d, i, a)
		this.forEach(unravel)
	}
	mapEachTone(fn){
		const unravel = (d, i, a)=> d instanceof Channel? d.mapEachTone(fn): fn(d, i, a)
		return new Channel(...this.map(unravel))
	}
	repeat(n){return new Channel(...Array(n).fill(this).flat())}
}

module.exports = Channel
