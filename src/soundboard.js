const Modulator = require('./modulator')

const hookup = (context, input, output, left, right, effects=[])=>{
	let current = input
	effects.forEach(effect=>{
		const node = effect.getNode(context)
		current.connect(node)
		current = node
	})
	if(left || right){
		const direction = left? 0: 1
		const splitter = context.createChannelSplitter(2)
		const merger = context.createChannelMerger(2)
		splitter.connect(current, direction)
		current.connect(merger, 0, direction)
		merger.connect(output)
	}else{
		current.connect(output)
	}
}
const instrument = (context, o, instrument)=>{
	if(!instrument) return
	if(typeof instrument === 'string') return o.type = instrument
	//caching legitimately reduces load up lag
	o.setPeriodicWave(instrument.wave || (instrument.wave = context.createPeriodicWave(instrument.real, instrument.imag)))
}
const modulate = async (param, modulator, time, base=Modulator.ZERO)=>{
	//Idk why but I need to use both methods, maybe some kind of lag?
	param.value = +base
	param.exponentialRampToValueAtTime(+base, time)
	const data = []
	for await(const {v, d, linear} of modulator.generator()){
		data.push({v: +v, d, linear})
		if(linear) param.linearRampToValueAtTime(+v, time = time + d)
		param.exponentialRampToValueAtTime(+v, time = time + d)
	}
	return new Modulator(...data)
}

module.exports = async (player, tone, time)=>{
	const o = player.context.createOscillator()
	const g = player.context.createGain()
	o.connect(g)
	const done = new Promise(a=> o.addEventListener('ended', a))

	hookup(player.context, g, player.compressor, tone.left, tone.right, tone.effects)
	instrument(player.context, o, tone.instrument)

	const start = time = time + tone.start
	o.start(start)
	const [pitch, volume] = await Promise.all([
		tone.pitch instanceof Modulator?
			modulate(o.frequency, tone.pitch, time, tone.pitch[0].v):
			(()=>{o.frequency.value = tone.pitch.hz})(),
		modulate(g.gain, tone.volume, time),
	])
	time = time + Math.max(pitch? pitch.duration: 0, volume.duration)
	o.stop(time + tone.extraDuration)

	return {
		start, end: time + tone.end,
		tone: {
			...tone, volume,
			pitch: pitch || {...tone.pitch}
		},
		done, absEnd: time + tone.extraDuration
	}
}
