class Complex{
	constructor(re, im=0){
		this.re = re
		this.im = im
	}
	add(c){return new Complex(this.re + c.re, this.im + c.im)}
	sub(c){return new Complex(this.re - c.re, this.im - c.im)}
	mul(c){return new Complex(
		this.re * c.re - this.im * c.im,
		this.re * c.im + this.im * c.re
	)}
	cexp(){
		const m = Math.exp(this.re)
		return new Complex(
			m * Math.cos(this.im),
			m * Math.sin(this.im)
		)
	}
}
const icfft = amplitudes=>{
	const data = cfft( //apply fourier transform
		amplitudes.map(a=> new Complex(a.re, -a.im)) //conjugate imaginary
	)
	data.forEach(a=>{
		a.im = -a.im //conjugate again
		a.re *= 1 / data.length //scale
		a.im *= 1 / data.length //scale
	})
	return data
}
const toComplex = v=> v instanceof Complex? v: new Complex(v)
const cfft = amplitudes=>{
	if(amplitudes.length <= 1) return amplitudes
	const hLength = amplitudes.length/2 |0
	const evens = cfft(Array.from({length: hLength}, (_, i)=> amplitudes[i*2])).map(toComplex)
	const odds = cfft(Array.from({length: hLength}, (_, i)=> amplitudes[i*2+1])).map(toComplex)

	const a = -2*Math.PI
	const data = []
	for(let i=0; i<hLength; i++){
		const even = evens[i]
		const odd = odds[i]
		let t = new Complex(0, a * (i/amplitudes.length))
		t = t.cexp().mul(odd)
		data[i] = even.add(t)
		data[i + hLength] = even.sub(t)
	}
	return data
}

module.exports = {Complex, icfft, cfft}
