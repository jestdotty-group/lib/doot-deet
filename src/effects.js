class Distortion{
	constructor(amount){
		this.amount = amount
	}
	getCurve(context){
		if(this.curve && this.curve.length === context.sampleRate) return this.curve
		const curve = new Float32Array(context.sampleRate)
		for(let i=0; i<curve.length; i++){
			const x = i * 2 / curve.length -1
			curve[i] = (3+this.amount) *x*20*(Math.PI/180)/ (Math.PI + this.amount * Math.abs(x))
		}
		return this.curve = curve
	}
	getNode(context){
		const wave = context.createWaveShaper()
		wave.curve = this.getCurve(context)
		return wave
	}
}
class Reverb{
	constructor(duration=1, decay=2, reverse){
		this.duration = duration
		this.decay = decay
		this.reverse = reverse
	}
	getBuffer(context){
		if(this.buffer && this.buffer.length === context.sampleRate) return this.buffer
		const buffer = context.createBuffer(2, context.sampleRate * this.duration, context.sampleRate)
		const left = buffer.getChannelData(0)
		const right = buffer.getChannelData(1)
		for(let i=0; i<buffer.length; i++){
			const delta = this.reverse? buffer.length - i: i
			left[i] = (Math.random() * 2 - 1) *Math.pow(1 - delta / buffer.length, this.decay)
			right[i] = (Math.random() * 2 - 1) *Math.pow(1 - delta / buffer.length, this.decay)
		}
		return this.buffer = buffer
	}
	getNode(context){
		const convolver = context.createConvolver()
		convolver.buffer = this.getBuffer(context)
		return convolver
	}
}

module.exports = {
	Distortion, Reverb
}
